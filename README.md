##Library##

Administration system for commercial library build with WPF.

+ User registration and authentication
+ Admin can add books to store, edit and delete books, close orders.
+ An authorized user has a cart and can make orders, wait in queue for missing book
+ There is a message system. User gets messages about actual order state and books he is waiting for
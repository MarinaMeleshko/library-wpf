﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Library.Model;

namespace Library.ViewModel
{
    public class LogInViewModel : ViewModel
    {
        private User _user;//? изменится ли юзер в арр
        private readonly List<User> _users;
        private readonly LogInWindow _logInWindow;

        public LogInViewModel(List<User> users, User user, LogInWindow window)
        {
            _user = user;
            _users = users;
            _logInWindow = window;
        }

        private string _logIn;

        public string Login
        {
            get => _logIn;
            set
            {
                _logIn = value;
                OnPropertyChanged();
            }
        }

        private string _message;

        public string Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged();
            }
        }

        private RelayCommand _signIn;

        public RelayCommand SignIn
        {
            get
            {
                return _signIn ?? (_signIn = new RelayCommand(obj =>
                {
                    var passwordBox = obj as PasswordBox;
                    if (passwordBox == null) return;
                    foreach (var u in _users)
                    {
                        if (u.Login == Login && u.Password == passwordBox.Password)
                        {
                            _user = u;                           
                            _logInWindow.Close();
                            if (Application.Current.MainWindow != null)
                                Application.Current.MainWindow.DataContext = new AppViewModel(u){Users = _users};
                            return;
                        }                           
                    }
                    Message = "Неправильный логин или пароль";
                }));
            }
        }
    }
}

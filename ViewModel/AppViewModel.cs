﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Model;

namespace Library.ViewModel
{
    public class AppViewModel : ViewModel
    {
        public User User { get; set; }
        public List<User> Users { get; set; } = new List<User>();
        public UserRole UserRole { get; }
        private MainViewModel _mainViewModel;

        public MainViewModel MainViewModel
        {
            get => _mainViewModel;
            set
            {
                _mainViewModel = value;
                OnPropertyChanged();
            }
        }

        private CartViewModel _cartViewModel;

        public CartViewModel CartViewModel
        {
            get => _cartViewModel;
            set
            {
                _cartViewModel = value;
                OnPropertyChanged();
            }
        }

        private HistoryViewModel _historyViewModel;

        public HistoryViewModel HistoryViewModel
        {
            get => _historyViewModel;
            set
            {
                _historyViewModel = value;
                OnPropertyChanged();
            }
        }

        public AppViewModel(User user = null)
        {
            var u = new User {Role = UserRole.Visiter, Login = "1", Password = "1"};//later read from file
            Users.Add(u);
            var us = new User {Role = UserRole.Admin, Login = "admin", Password = "admin"};
            Users.Add(us);
            User = user;
            if (user != null)
            {
                UserRole = user.Role;
            }
            MainViewModel = new MainViewModel(User);
            CartViewModel = new CartViewModel(User);
            HistoryViewModel = new HistoryViewModel(User);
        }

        private RelayCommand _logIn;

        public RelayCommand LogIn
        {
            get
            {
                return _logIn ?? (_logIn = new RelayCommand(obj =>
                {
                    new LogInWindow(Users, User).Show();
                }));
            }
        }

        private RelayCommand _register;

        public RelayCommand Register
        {
            get
            {
                return _register ?? (_register = new RelayCommand(obj =>
                {
                    var w = new RegisterWindow();
                    w.DataContext = new RegisterViewModel(Users, w);
                    w.Show();
                }));
            }
        }
    }
}

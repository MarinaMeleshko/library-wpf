﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Library.Model;

namespace Library.ViewModel
{
    public class CartViewModel : ViewModel
    {
        public User User { get; }//может быть для привязки нужен сеттер
        private ObservableCollection<Book> _books;
        public List<Book> SelectedBooks { get; } = new List<Book>();

        public CartViewModel(User user)
        {
            User = user;
            if(User!=null)
                _books = User.Cart;
        }

        public ObservableCollection<Book> Books
        {
            get => _books;
            set
            {
                _books = value;
                OnPropertyChanged();
            }
        }

        private RelayCommand _registerOrder;

        public RelayCommand RegisterOrder
        {
            get
            {
                return _registerOrder ?? (_registerOrder = new RelayCommand(obj =>
                {
                    var order = new Order(User, 21, SelectedBooks.ToArray());
                    MessageBox.Show("Заказ " + order.Id + " оформлен", "Заказ " + order.Id, MessageBoxButton.OK);
                    foreach (var book in SelectedBooks)
                    {
                        _books.Remove(book);
                        User.Cart.Remove(book);
                    }
                    SelectedBooks.Clear();
                    //add to orders collection for admin
                }, obj => (SelectedBooks.Count > 0)));
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Model;

namespace Library.ViewModel
{
    public class BookViewModel : ViewModel
    {
        public Book Book { get; set; }
        private readonly ObservableCollection<Book> _books;

        public BookViewModel(Book book, ObservableCollection<Book> books)
        {
            Book = book;
            _books = books;
        }

        private RelayCommand _save;

        public RelayCommand Save
        {
            get
            {
                return _save ?? (_save = new RelayCommand(obj =>
                {
                    if (Book.AvailableExemplarsCount > 0)
                        Book.IsAvailable = true;
                    if(!_books.Contains(Book))
                        _books.Add(Book);
                }));
            }
        }
    }
}

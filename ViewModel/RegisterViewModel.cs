﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using Library.Model;

namespace Library.ViewModel
{
    public class RegisterViewModel : ViewModel
    {
        private readonly List<User> _users;
        private readonly RegisterWindow _registerWindow;

        private string _message;

        public string Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged();
            }
        }

        private string _login = "";

        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged();
            }
        }

        private string _firstName = "";

        public string FirstName
        {
            get => _firstName;
            set
            {
                _firstName = value;
                OnPropertyChanged();
            }
        }

        private string _lastName = "";

        public string LastName
        {
            get => _lastName;
            set
            {
                _lastName = value;
                OnPropertyChanged();
            }
        }

        private string _addres = "";

        public string Addres
        {
            get => _addres;
            set
            {
                _addres = value;
                OnPropertyChanged();
            }
        }

        private string _phone = "";

        public string Phone
        {
            get => _phone;
            set
            {
                _phone = value;
                OnPropertyChanged();
            }
        }

        private string _email = "";

        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }

        public RegisterViewModel(List<User> users, RegisterWindow window)
        {
            _users = users;
            _registerWindow = window;
        }

        private RelayCommand _register;

        public RelayCommand Register
        {
            get
            {
                return _register ?? (_register = new RelayCommand(obj =>
                {
                    if (_users.Any(u => u.Login == Login))
                    {
                        Message = "Пользователь с таким логином уже существует";
                        return;
                    }
                    var user = new User
                    {
                        Login = Login,
                        FirstName = FirstName,
                        LastName = LastName,
                        Addres = Addres,
                        EMail = Email,
                        Phone = Phone,
                        Password = _registerWindow.Password.Password
                    };
                    _users.Add(user);
                    _registerWindow.Close();
                    if (Application.Current.MainWindow != null)
                    {
                        Application.Current.MainWindow.DataContext = new AppViewModel(user){ Users = _users };
                    }
                },
                (obj) => 
                    _registerWindow.Password.Password == _registerWindow.RepeatPassword.Password
                    && _registerWindow.Password.Password != "" && !string.IsNullOrEmpty(Login) 
                    && new Regex(@"^[a-zA-Z]+$").IsMatch(FirstName)
                    && new Regex(@"^[a-zA-Z]+$").IsMatch(LastName)
                    && new Regex(@"^\+375[0-9]{2}[0-9]{7}$").IsMatch(Phone)
                    && new Regex(@"^\w+\@\w+\.\w+$").IsMatch(Email)));
            }
        }
    }
}

﻿using System.Collections.Generic;
using Library.Model;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Json;

namespace Library.ViewModel
{   
    public class MainViewModel : ViewModel
    {
        public User User { get; }
        private ObservableCollection<Book> _books;
        private const string BooksFilePath = "books.json";
        private const string LoginUserPath = "login.json";

        public UserRole UserRole { get; }

        public MainViewModel(User user)
        {    
            var jsonFormatter = new DataContractJsonSerializer(typeof(ObservableCollection<Book>));
            using (var fs = new FileStream(BooksFilePath, FileMode.OpenOrCreate))
            {
                _books = new ObservableCollection<Book>
                {
                    new Book{Title = "Война и мир", Authors = {"Лев Толстой"}, Cost = 20, Pledge = 5, ExemplarsCount = 10, AvailableExemplarsCount = 10, IsAvailable = true, Genres = {Genre.Fiction, Genre.History, Genre.Romance}},
                    new Book{Title="Алмазная колесница", Authors = {"Борис Акунин"}, Cost = 18, Pledge = 3, ExemplarsCount = 16, AvailableExemplarsCount = 16, IsAvailable = true, Genres = {Genre.Fiction, Genre.Romance, Genre.Series}},
                    new Book{Title = "Приглашение на казнь", Authors = {"Владимир Набоков"}, Cost=12, Pledge = 5, ExemplarsCount = 6, AvailableExemplarsCount = 1, IsAvailable = true, Genres={Genre.Fiction, Genre.Religious}}
                };
                jsonFormatter.WriteObject(fs, _books);
            }
            using (var fs = new FileStream(BooksFilePath, FileMode.OpenOrCreate))
            {
                _books = (ObservableCollection<Book>) jsonFormatter.ReadObject(fs);
            }
            User = user;
            if(user!=null)
                UserRole = user.Role;
        }

        public ObservableCollection<Book> Books
        {
            get => _books;
            set
            {
                _books = value;
                OnPropertyChanged();
            }
        }


        public void Close()
        {
            var jsonFormatter = new DataContractJsonSerializer(typeof(ObservableCollection<Book>));
            using (var fs = new FileStream(BooksFilePath, FileMode.OpenOrCreate))
            {
                jsonFormatter.WriteObject(fs, _books);
            }
            //write user;
        }


        private RelayCommand _addToCart;

        public RelayCommand AddToCart
        {
            get
            {
                return _addToCart ?? (_addToCart = new RelayCommand(obj =>
                {
                    if (obj is Book book)
                        User.Cart.Add(book);
                }));
            }
        }

    }
}

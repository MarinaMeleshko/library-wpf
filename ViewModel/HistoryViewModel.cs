﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Model;

namespace Library.ViewModel
{
    public class HistoryViewModel : ViewModel
    {
        public User User { get; }
        private readonly ObservableCollection<Order> _orders = new ObservableCollection<Order>();
        private ObservableCollection<Order> _displayedOrders;
        private List<OrdersStatus> _displayedOrdersStatus;

        public HistoryViewModel(User user)
        {
            User = user;
            if(user!=null)
                _orders = user.Orders;
            _displayedOrders = new ObservableCollection<Order>(_orders);
            _displayedOrdersStatus =
                new List<OrdersStatus> {OrdersStatus.Actual, OrdersStatus.Overdue, OrdersStatus.Returned};
            _orders.CollectionChanged += (sender, arg) =>
            {
                if (arg.Action != NotifyCollectionChangedAction.Add) return;
                var order = arg.NewItems[0] as Order;
                if (order != null && _displayedOrdersStatus.Contains(order.Status))
                    _displayedOrders.Add(order);
            };
        }

        //public ObservableCollection<Order> Orders
        //{
        //    get => _orders;
        //    set
        //    {
        //        _orders = value;
        //        OnPropertyChanged();
        //    }
        //}

        public ObservableCollection<Order> DisplayedOrders
        {
            get => _displayedOrders;
            set
            {
                _displayedOrders = value;
                OnPropertyChanged();
            }
        }

        public void SelectOrders(List<OrdersStatus> status)
        {
            var orders = new ObservableCollection<Order>();
            foreach (var order in _orders)
            {
                if(status.Contains(order.Status))
                    orders.Add(order);
            }
            DisplayedOrders = orders;
            _displayedOrdersStatus = status;
        }
    }
}

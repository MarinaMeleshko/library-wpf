using Library.Model;
using Library.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Library
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public UserRole UserRole { get; }
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new AppViewModel();
            UserRole = ((AppViewModel) DataContext).UserRole;
        }



        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            foreach (Window w in Application.Current.Windows)
            {
                w.Close();
            }
            //var mainViewModel = DataContext as MainViewModel;
            //mainViewModel?.Close();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            if (btn == null) return;
            if (!(btn.DataContext is Book book)) return;
            var vm = DataContext as AppViewModel;
            if (vm?.User == null)
            {
                MessageBox.Show("������� ��� �����������������");
                return;
            }
            if (vm.User.Cart.Contains(book)) return;
            vm.User.Cart.Add(book);
        }

        private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            if (!(sender is CheckBox checkBox)) return;
            var book = checkBox.DataContext as Book;
            if (book == null) return;
            (DataContext as AppViewModel)?.CartViewModel.SelectedBooks.Add(book);
        }

        private void ToggleButton_OnUnchecked(object sender, RoutedEventArgs e)
        {
            if (!(sender is CheckBox checkBox)) return;
            var book = checkBox.DataContext as Book;
            if (book == null) return;
            (DataContext as AppViewModel)?.CartViewModel.SelectedBooks.Remove(book);
        }

        private void AddToWaitingList_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button btn)) return;
            if (!(DataContext is AppViewModel vm)) return;
            if (!(btn.DataContext is Book book)) return;
            book.AddUserToQueue(vm.User);
        }

        private void RemoveFromCart_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button btn)) return;
            if (!(DataContext is AppViewModel vm)) return;
            if (!(btn.DataContext is Book book)) return;
            vm.CartViewModel.Books.Remove(book);
            vm.User.Cart.Remove(book);
        }

        private void Checked(object sender, RoutedEventArgs e)
        {
            if (All.IsChecked == false && Actual.IsChecked == false &&
                Overdue.IsChecked == false && Returned.IsChecked == false)
                return;
            var vm = (DataContext as AppViewModel)?.HistoryViewModel;
            if (vm == null) return;
            if (All.IsChecked == true)
            {
                vm.SelectOrders(
                    new List<OrdersStatus> { OrdersStatus.Actual, OrdersStatus.Overdue, OrdersStatus.Returned });
                return;
            }
            var status = new List<OrdersStatus>();
            if (Actual.IsChecked == true)
            {
                status.Add(OrdersStatus.Actual);
            }
            if (Overdue.IsChecked == true)
            {
                status.Add(OrdersStatus.Overdue);
            }
            if (Returned.IsChecked == true)
            {
                status.Add(OrdersStatus.Returned);
            }
            vm.SelectOrders(status);
        }

        private void CloseOrder_OnClick(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var order = btn?.DataContext as Order;
            order?.Close();
        }

        private void RemoveMessage_OnClick(object sender, RoutedEventArgs e)
        {
            var message = (sender as Button)?.DataContext as Message;
            if (message == null) return;
            var u = (DataContext as AppViewModel)?.User;
            u?.Messages.Remove(message);
        }

        private void Edit_OnClick(object sender, RoutedEventArgs e)
        {
            var b = (sender as Button)?.DataContext as Book;
            var books = (DataContext as AppViewModel)?.MainViewModel.Books;
            if (books == null || b == null) return;
            new BookWindow(b, books).Show();
        }

        private void Add_OnClick(object sender, RoutedEventArgs e)
        {
            var books = (DataContext as AppViewModel)?.MainViewModel.Books;
            if (books == null) return;
            new BookWindow(new Book(), books).Show();
        }

        private void RemoveBook_OnClick(object sender, RoutedEventArgs e)
        {
            var book = (sender as Button)?.DataContext as Book;
            var collection = (DataContext as AppViewModel)?.MainViewModel?.Books;
            if (book == null || collection == null)
                return;
            collection.Remove(book);
        }

        private void LogOut_OnClick(object sender, RoutedEventArgs e)
        {
            var users = (DataContext as AppViewModel)?.Users;
            DataContext = new AppViewModel{Users = users};
        }
    }
}
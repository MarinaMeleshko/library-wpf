﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace Library.Model
{
    [DataContract]
    public class Book : INotifyPropertyChanged
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember] private string _title;
        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }
        [DataMember]
        public List<string> Authors { get; set; } = new List<string>();

        [DataMember] private int _exemplarsCount;

        public int ExemplarsCount
        {
            get => _exemplarsCount;
            set
            {
                _exemplarsCount = value;
                OnPropertyChanged();
            }
        }

        private bool _isAvailable;
        [DataMember]
        public bool IsAvailable
        {
            get => _isAvailable;
            set
            {
                _isAvailable = value;
                OnPropertyChanged();
            }

        }

        [DataMember] private int _availableExemplarsCount;

        public int AvailableExemplarsCount
        {
            get => _availableExemplarsCount;
            set
            {
                _availableExemplarsCount = value;
                OnPropertyChanged();
            }
        }

        [DataMember] private double _cost;

        public double Cost
        {
            get => _cost;
            set
            {
                _cost = value;
                 OnPropertyChanged();
            }
        }

        [DataMember] private double _pledge;

        public double Pledge
        {
            get => _pledge;
            set
            {
                _pledge = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        public List<User> UsersQueue { get; set; } = new List<User>();
        [DataMember]
        public List<Genre> Genres { get; set; } = new List<Genre>();
        [DataMember]
        public List<Order> Orders { get; set; } = new List<Order>();
        public Book()
        {
            Id = new Random().Next(100000, 999999);
        }

        public void FreeExemplar()
        {
            foreach (var user in UsersQueue)
            {
                Messenger.SendMessage(this, user, "Книга доступна к заказу");
            }
            AvailableExemplarsCount++;
            if (!IsAvailable)
                IsAvailable = true;
        }

        public void AddUserToQueue(User user)
        {
            if (!UsersQueue.Contains(user))
            {
                UsersQueue.Add(user);
                Messenger.SendMessage(this, user, "Вы в очереди ожидания этой книги");
            }
        }

        public void RemoveUserFromQueue(User user)
        {
            if (UsersQueue.Contains(user))
            {
                UsersQueue.Remove(user);
                Messenger.SendMessage(this, user, "Вы вышли из очереди ожидания этой книги");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public override string ToString()
        {
            return Title;
        }
    }
}

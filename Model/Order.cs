﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace Library.Model
{
    [DataContract]
    public class Order : INotifyPropertyChanged
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public User User { get; set; }
        [DataMember]
        public List<Book> Books { get; set; }

        [DataMember]
        private OrdersStatus _status;
        public OrdersStatus Status
        {
            get => _status;
            set
            {
                _status = value;
                OnPropertyChanged();
            }
        }
        [DataMember]
        public double Cost { get; set; }
        [DataMember]
        public double Pledge { get; set; }
        [DataMember]
        public DateTime DateOfBegin { get; set; }

        [DataMember] private DateTime _dateOfReturning;

        public DateTime DateOfReturning
        {
            get => _dateOfReturning;
            private set
            {
                _dateOfReturning = value;
                OnPropertyChanged();
            }
        }
        [DataMember]
        public bool IsOverdue { get; private set; }
        [DataMember]
        public int Delay { get; private set; }
        [DataMember]
        public double DelayCost { get; private set; }

        public Order(User user, int days, params Book[] books)
        {
            Id = new Random().Next(100000, 999999);
            Books = new List<Book>();
            foreach (var book in books)
            {
                book.AvailableExemplarsCount--;
                if (book.AvailableExemplarsCount == 0)
                    book.IsAvailable = false;
                book.Orders.Add(this);
                Books.Add(book);
                if (book.UsersQueue.Contains(user))
                    book.UsersQueue.Remove(user);
            }

            User = user;
            User.Orders.Add(this);

            Status = OrdersStatus.Actual;
            Pledge = Books.Sum(b => b.Pledge);
            DateOfBegin = DateTime.Today.Date;
            DateOfReturning = DateTime.Today.AddDays(days).Date;
            Cost = Books.Sum(b => b.Cost) * (DateOfReturning - DateOfBegin).Days;
        }

        public void Refresh()
        {
            if (Status == OrdersStatus.Returned)
                return;
            if (Status == OrdersStatus.Overdue)
            {
                Delay++;
                DelayCost += Books.Sum(b => b.Cost);
                return;
            }
            if (DateTime.Today <= DateOfReturning) return;
            Status = OrdersStatus.Overdue;
            IsOverdue = true;
        }

        public void Close()
        {
            DateOfReturning = DateTime.Today;
            Status = OrdersStatus.Returned;
            foreach (var book in Books)
            {
                book.FreeExemplar();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}

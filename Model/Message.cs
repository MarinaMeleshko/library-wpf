﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model
{
    public class Message
    {
        public object Sender { get; set; }
        public string Text { get; set; }

        public Message(object sender, string text)
        {
            Sender = sender;
            Text = text;
        }
    }
}

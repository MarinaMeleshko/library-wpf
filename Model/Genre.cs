﻿namespace Library.Model
{
    public enum Genre
    {
        Fiction, NonFiction, ScienceFiction,
        Fantasy, BusinesFinance, Politics, TravelBooks,
        Autobiography, History, Triller, Romance, Satire,
        Horror, Religious, HealthMedicine, CookBooks,
        ChildrensBooks, Dictionary, Encyclopedia,
        Series, Anthology
    }

}

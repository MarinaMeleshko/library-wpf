﻿namespace Library.Model
{
    public enum OrdersStatus
    {
        Actual, Overdue, Returned
    }
}

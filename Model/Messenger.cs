﻿namespace Library.Model
{
    public static class Messenger
    {
        public static void SendMessage(object sender, User addressee, string message)
        {
            addressee.Messages.Add(new Message(sender, message));
        }
    }
}
